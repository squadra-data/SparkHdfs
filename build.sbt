

name := "SparkHdfs"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
                      "org.apache.spark" %% "spark-sql" % "2.3.2" % "provided",
                      "org.scalactic" %% "scalactic" % "3.0.8",
                      "org.scalatest" %% "scalatest" % "3.0.8",
                      "org.elasticsearch" %% "elasticsearch-spark-20" % "6.6.2",
                       "org.elasticsearch" % "elasticsearch-hadoop" % "6.6.2"
                            )


