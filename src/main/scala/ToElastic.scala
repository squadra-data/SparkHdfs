import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.spark._


object ToElastic extends App with SharedSparkSession {
  /*
  val conf = new SparkConf().setAppName("appName").setMaster("master")
  conf.set("es.index.auto.create", "true")
  val sc=new SparkContext(conf)
  val numbers=Map("one"->1, "tow" -> 2, "three" ->3 )
  val airports= Map("arrival" -> "Otopeni", "SFO" -> "San Fran")

  sc.makeRDD(Seq(numbers, airports)).saveToEs("Spark/docs")*/
  val df =sparkIn.read.option("header", "true").csv("http://172.16.10.36/data.csv")
  df.write
    .format("org.elasticsearch.spark.sql")
    .option("es.nodes.wan.only","true")
    .option("es.port","1027")
    .option("es.net.ssl","true")
    .option("es.nodes", "master-1-node.elastic.autoip.dcos.thisdcos.directory")
    .mode("Overwrite")
    .save("index/dogs")
}
