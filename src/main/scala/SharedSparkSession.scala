import org.apache.spark.sql.{DataFrame, SparkSession}

trait SharedSparkSession {
  val sparkIn = SparkSession.builder.appName("Application").master("local").getOrCreate()
}
